#!/usr/bin/env python3

"""
Main entry point for pyChargelio module.
"""

import pyChargelio

pyChargelio.runPyChargelio()
