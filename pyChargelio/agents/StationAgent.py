"""
Provides the StationAgent class.
"""
import json

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour
from spade.message import Message
from spade.template import Template

class StationAgent(Agent):
	"""Agent for the charging station"""

	class GenerateChargingOfferBehaviour(CyclicBehaviour):
		"""
		Behaviour run at message retrieval;
		Generates station information
		"""

		async def run(self):
			offerRequest = await self.receive(timeout=20)
			if not offerRequest:
				return

			sender = str(offerRequest.sender)
			body = str(offerRequest.body)
			print("[StationAgent] Offer request received")

			data = json.loads(body)

			vehicleStandards = data["vehicleMetadata"]["chargingStandards"]
			offer = Message(to=sender)

			offerBody = {
				"vehicleSupported": (self.agent.stationChargingStandard in vehicleStandards),
				"freeChargingSpot": (self.agent.freeChargingSpots > 0),
				"chargingOffer": {
					"standard": self.agent.stationChargingStandard,
					"price": self.agent.stationChargingPrice,
					"power": self.agent.chargingPower,
					"freeChargingSpots": self.agent.freeChargingSpots
				}
			}
			offerOK = (self.agent.freeChargingSpots > 0) and (self.agent.stationChargingStandard in vehicleStandards)
			offer.set_metadata("performative", "propose" if offerOK else "refuse")

			offer.body = json.dumps(offerBody)

			await self.send(offer)
			print("[StationAgent] Charging {} sent".format("offer" if offerOK else "refusal"))

	class ReceiveOfferAcceptanceBehaviour(CyclicBehaviour):
		"""
		Behaviour run at message retrieval;
		Generates station information
		"""

		async def run(self):
			offerAcceptance = await self.receive(timeout=20)
			if not offerAcceptance:
				return

			# Accepting an offer is treated like taking up 1 spot
			self.agent.freeChargingSpots = self.agent.freeChargingSpots - 1

			print("[StationAgent] Offer acceptance received with content: {}".format(offerAcceptance.body))

	class ReceiveOfferRejectionBehaviour(CyclicBehaviour):
		"""
		Behaviour run at message retrieval;
		Generates station information
		"""

		async def run(self):
			offerRejection = await self.receive(timeout=20)
			if not offerRejection:
				return

			print("[StationAgent] Offer rejection received with content: {}".format(offerRejection.body))

	class KillAgentAfterTimeBehaviour(PeriodicBehaviour):
		"""
		Import charging stations from the file periodically - every n seconds (passed as an argument). Default value is 1.
		Also cares about time-to-live for server.
		Stops the server after time (passed as an argument) that elapsed from start (in seconds).
		Default value is 0 meaning infinite work of server.
		"""

		async def run(self):
			if self.agent.ttl != 0:
				if self.agent.counter >= self.agent.ttl:
					print("[StationAgent] TTL reached")
					self.kill()
				self.agent.counter += 1

		async def on_end(self):
			self.agent.deleteStationFromServerFile()
			# stop agent
			self.agent.stop()

		async def on_start(self):
			self.agent.counter = 0

	def __init__(self, *args, **kwargs):
		"""
		extra args:
		* stationListFilename: stationListFilename (default: "stationLocationList.csv")
		* positionX: X coordinate of the station's position (default 0)
		* positionY: Y coordinate of the station's position (default 0)
		* chargingStandard: station's charging standard (defalt "std1")
		* numberOfSpots: number of charging/parking spots (default 0)
		* chargingPrice: chargingPrice (default 1.0)
		* chargingPower: chargingPower (default 1000)
		* ttl: time-to-live, in seconds, zero means no ttl (default: 0)
		"""

		self.stationListFilename = kwargs.pop("stationListFilename", "stationLocationList.csv")
		self.stationPositionX = float(kwargs.pop("positionX", 0))
		self.stationPositionY = float(kwargs.pop("positionY", 0))
		self.stationChargingStandard = kwargs.pop("chargingStandard", "std1")
		self.stationNumberOfSpots = int(kwargs.pop("numberOfSpots", 0))
		self.stationChargingPrice = float(kwargs.pop("chargingPrice", 0))
		self.chargingPower = float(kwargs.pop("chargingPower", 1000))
		self.ttl = int(kwargs.pop("ttl", 0))
		self.counter = 0
		self.freeChargingSpots = self.stationNumberOfSpots

		# Can't use super() due to use of kwargs
		Agent.__init__(self, *args, **kwargs)

	def addStationToList(self):
		"""
		Adds this station to ServerAgent's station list.
		"""

		with open(self.stationListFilename, "a") as stationListFile:
			stationInfo = "{},{},{}\n".format(self.jid, self.stationPositionX, self.stationPositionY)
			stationListFile.write(stationInfo)
			stationListFile.close()

	def deleteStationFromServerFile(self):
		"""
		Delete this station from ServerAgent's station list.
		"""

		with open(self.stationListFilename, "r+") as stationListFile:
			lines = stationListFile.readlines()
			stationListFile.seek(0)
			for line in lines:
				(stationID, _, _) = tuple(line.split(','))
				if stationID != self.jid:
					stationListFile.write(line)
			stationListFile.truncate()
			stationListFile.close()

	def setup(self):
		print("[StationAgent] setup!")
		print("[StationAgent] Number of available spots: {}".format(self.freeChargingSpots))

		self.addStationToList()

		gcoTemplate = Template()
		gcoTemplate.to = str(self.jid)
		gcoTemplate.set_metadata("performative", "cfp")

		roaTemplate = Template()
		roaTemplate.to = str(self.jid)
		roaTemplate.set_metadata("performative", "accept-proposal")

		rorTemplate = Template()
		rorTemplate.to = str(self.jid)
		rorTemplate.set_metadata("performative", "reject-proposal")

		# Add periodic UpdateStations Behaviour
		kaatBehaviour = self.KillAgentAfterTimeBehaviour(period=1)
		self.add_behaviour(kaatBehaviour)

		gcoBehaviour = self.GenerateChargingOfferBehaviour()
		self.add_behaviour(gcoBehaviour, gcoTemplate)

		roaBehaviour = self.ReceiveOfferAcceptanceBehaviour()
		self.add_behaviour(roaBehaviour, roaTemplate)

		rorBehaviour = self.ReceiveOfferRejectionBehaviour()
		self.add_behaviour(rorBehaviour, rorTemplate)
