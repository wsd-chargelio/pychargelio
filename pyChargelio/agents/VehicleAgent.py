"""
Provides the VehicleAgent class.
"""

import json

from spade.agent import Agent
from spade.behaviour import OneShotBehaviour
from spade.message import Message


class VehicleAgent(Agent):
	"""Agent for the electric vehicle"""

	class RequestStationListBehaviour(OneShotBehaviour):
		"""Behaviour run at agent's wake-up;
		Queries the station location server for a list of stations meeting location requirements
		"""

		async def run(self):
			print("[VehicleAgent] RequestStationListBehaviour starting")
			message = Message(to=self.agent.serverAgentJID)

			# Set the "request" FIPA performative
			message.set_metadata("performative", "request")

			# Clear agent's localStationList
			self.agent.localStationList = []

			# TODO: configurable maximum search radius
			while self.agent.searchCounter < 10 and not self.agent.localStationList:
				# Set the message content
				data = {
					"vehicleMetadata": {
						"position": {
							"x": self.agent.vehiclePositionX,
							"y": self.agent.vehiclePositionY,
						}
					},
					"searchRadius": self.agent.searchRadius
				}
				message.body = json.dumps(data)

				# Send the message
				await self.send(message)
				print("[VehicleAgent] RequestStationList sent with radius {}".format(self.agent.searchRadius))

				# Receive message
				receivedMessage = await self.receive(timeout=20)

				if not receivedMessage:
					print("[VehicleAgent] Did not receive a response to RequestStationList!")
					break

				# Set localStationList from received message
				receivedMessageJson = json.loads(receivedMessage.body)
				self.agent.localStationList = receivedMessageJson["stations"]

				# Increment search counter and search radius
				self.agent.searchCounter += 1
				self.agent.searchRadius = self.agent.searchRadius * 1.5

				# If stations found, break the loop
				if self.agent.localStationList:
					break

			if self.agent.localStationList:
				print("[VehicleAgent] Found {} station(s)".format(len(self.agent.localStationList)))
				self.agent.rcoBehaviour = self.agent.RequestChargingOfferBehaviour()
				self.agent.add_behaviour(self.agent.rcoBehaviour)
			else:
				print("[VehicleAgent] Stations not found")
				self.agent.stop()

	class RequestChargingOfferBehaviour(OneShotBehaviour):
		"""Behaviour run when agent receives not empty stationList from ServerAgent
		Sends requests to all stations from received stationList"""

		async def run(self):
			# Clear stationOffers
			self.agent.stationOffers = []

			# Send messages to stations
			for stationJID in self.agent.localStationList:
				msg = Message(to=stationJID)
				msg.set_metadata("performative", "cfp")

				# Set the message content
				data = {
					"vehicleMetadata": {
						"chargingStandards": self.agent.chargingStandards
					}
				}
				msg.body = json.dumps(data)

				# Send the message
				await self.send(msg)
				print("[VehicleAgent] RequestChargingOffer message sent")

				# Receive the message
				receivedMessage = await self.receive(timeout=20)

				# Add offer to offer list
				if not receivedMessage:
					print("[VehicleAgent] Did not receive a response to RequestChargingOffer!")
					continue

				receivedOffer = json.loads(receivedMessage.body)
				# Add sender JID to the offer (without JID resource)
				receivedOffer['stationJID'] = str(receivedMessage.sender).split('/')[0]
				self.agent.stationOffers.append(receivedOffer)

			print("[VehicleAgent] Got {} charging offers".format(len(self.agent.stationOffers)))

			if self.agent.stationOffers:
				self.agent.ccoBehaviour = self.agent.ConfirmChargingOfferBehaviour()
				self.agent.add_behaviour(self.agent.ccoBehaviour)
			else:
				print("[VehicleAgent] Station offers not received, increasing search radius")
				self.agent.rslBehaviour = self.agent.RequestStationListBehaviour()
				self.agent.add_behaviour(self.agent.rslBehaviour)

	class ConfirmChargingOfferBehaviour(OneShotBehaviour):
		"""Behaviour run when agent receives offers from requested stations
		Accepts or rejects offers"""

		def __init__(self, *args, **kwargs):
			self.offers = []

			OneShotBehaviour.__init__(self, *args, **kwargs)

		async def run(self):
			self.offers = [
				offer
				for offer
				in self.agent.stationOffers
				if offer.get("chargingOffer").get("standard") in self.agent.chargingStandards
			]

			# Find best offer
			self.agent.acceptedOffer = self.findBestOffer()

			# Accept or reject other offers
			for stationJID in self.agent.localStationList:
				msg = Message(to=stationJID)

				if self.agent.acceptedOffer and self.agent.acceptedOffer.get("stationJID") == stationJID:
					# Set performative
					msg.set_metadata("performative", "accept-proposal")

					# Set the message content
					data = {
						"offerAccepted": True,
						"vehicleMetadata": {
							"position": {
								"x": self.agent.vehiclePositionX,
								"y": self.agent.vehiclePositionY
							},
							"requiredCharge": self.agent.requiredCharge
						}
					}
					msg.body = json.dumps(data)

					# Send the message
					await self.send(msg)
					print("[VehicleAgent] AcceptChargingOffer message sent")
				else:
					# Set performative
					msg.set_metadata("performative", "reject-proposal")

					# Set the message content
					data = {
						"offerAccepted": False,
						"vehicleMetadata": {}
					}
					msg.body = json.dumps(data)

					# Send the message
					await self.send(msg)
					print("[VehicleAgent] RejectChargingOffer message sent")

			if self.agent.acceptedOffer:
				print("[VehicleAgent] Charging station found, vehicle agent over'n'out!")
				self.agent.stop()
			else:
				print("[VehicleAgent] No suitable offers received, increasing search radius")
				self.agent.rslBehaviour = self.agent.RequestStationListBehaviour()
				self.agent.add_behaviour(self.agent.rslBehaviour)

		def findBestOffer(self):
			"""
			Finds best offer from offers with supported standard
			"""

			# TODO change implementation
			if len(self.offers):
				weights = {
					"price": 0.8,
					"power": 0.01,
					"freeChargingSpots": 1
				}

				for offer in self.offers:
					# Calculate weighted values for offer
					price = offer["chargingOffer"]["price"] * weights["price"]
					power = offer["chargingOffer"]["power"] * weights["power"]
					freeChargingSpots = offer["chargingOffer"]["freeChargingSpots"] * weights["freeChargingSpots"]

					offer["weightedValue"] = price + power + freeChargingSpots

				# Sort offers
				self.offers = sorted(self.offers, key=lambda offer: offer["weightedValue"])

				print("[VehicleAgent] AcceptChargingOffer - best offer found")
				return self.offers[0]

			print("[VehicleAgent] AcceptChargingOffer - best offer not found")
			return None

	def __init__(self, *args, **kwargs):
		"""
		extra args:
		* serverAgentJID: JID of the station location server (default: "ls@localhost")
		* vehiclePositionX: vehicle's x coordinate (default: 0)
		* vehiclePositionY: vehicle's y coordinate (default: 0)
		* searchRadius: radius to search for stations in (default: 10)
		* requiredCharge: battery charge required by the vehicle ,ie how much we want to charge (default: 50)
		* chargingStandards: vehicle's available standards, separated by commas, no spaces (default: to "std1")

		attributes:
		* localStationList: list of stations returned by ServerAgent
		* stationOffers: list of offers sent by requested StationAgents
		* acceptedOffer: best offer chosen by VehicleAgent
		"""

		self.serverAgentJID = kwargs.pop("serverAgentJID", "ls@localhost")
		self.vehiclePositionX = float(kwargs.pop("vehiclePositionX", 0))
		self.vehiclePositionY = float(kwargs.pop("vehiclePositionY", 0))
		self.searchRadius = float(kwargs.pop("searchRadius", 10))
		self.requiredCharge = float(kwargs.pop("requiredCharge", 50))
		chargingStandardsString = kwargs.pop("chargingStandards", "std1")
		self.chargingStandards = chargingStandardsString.split(',')
		self.localStationList = []
		self.stationOffers = []
		self.acceptedOffer = None
		self.searchCounter = 0

		# Can't use super() due to use of kwargs
		Agent.__init__(self, *args, **kwargs)

	def setup(self):
		print("[VehicleAgent] setup")

		self.rslBehaviour = self.RequestStationListBehaviour()
		self.add_behaviour(self.rslBehaviour)

		self.rcoBehaviour = None
		self.ccoBehaviour = None
