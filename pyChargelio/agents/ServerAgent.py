"""
Provides the ServerAgent class.
"""

import json
import math

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour
from spade.message import Message
from spade.template import Template


class ServerAgent(Agent):
	"""Agent for the station location server"""

	class GenerateStationListBehaviour(CyclicBehaviour):
		"""
		Behaviour run at message retrieval;
		Generates station list matching queried location and distance
		"""

		def findStations(self, vehiclePositionX, vehiclePositionY, searchRadius):
			"""
			Filter available stations based on requested position and radius.
			"""

			stations = []

			for stationTuple in self.agent.chargingStations:
				stationPositionX = float(stationTuple[1])
				stationPositionY = float(stationTuple[2])

				# hypot = sqrt(x**2 + y**2)
				distance = math.hypot(stationPositionX - vehiclePositionX, stationPositionY - vehiclePositionY)
				if distance <= searchRadius:
					stations.append(stationTuple[0])

			return stations

		async def run(self):
			message = await self.receive(timeout=20)
			if not message:
				return

			sender = str(message.sender)
			data = json.loads(message.body)

			vehiclePositionX = float(data["vehicleMetadata"]["position"]["x"])
			vehiclePositionY = float(data["vehicleMetadata"]["position"]["y"])
			searchRadius = float(data["searchRadius"])

			stations = self.findStations(vehiclePositionX, vehiclePositionY, searchRadius)

			# send response to the agent that sent the request
			response = Message(to=sender)
			# Set the "inform" FIPA performative
			response.set_metadata("performative", "inform")
			response.body = json.dumps({"stations": stations})

			await self.send(response)
			print("[ServerAgent] Station list ({} station(s)) sent".format(len(stations)))

	class UpdateStationsBehaviour(PeriodicBehaviour):
		"""
		Import charging stations from the file periodically - every n seconds (passed as an argument). Default value is 1.
		Also cares about time-to-live for server.
		Stops the server after time (passed as an argument) that elapsed from start (in seconds).
		Default value is 0 meaning infinite work of server.
		"""

		async def run(self):
			if self.agent.ttl != 0:
				if self.agent.counter >= self.agent.ttl:
					print("[ServerAgent] TTL reached")
					self.kill()
				self.agent.counter += 1

			if divmod(self.agent.counter, self.agent.updateFrequency)[1] == 0:
				self.agent.readStationsFromFile()

		async def on_end(self):
			# stop agent from behaviour
			self.agent.stop()

		async def on_start(self):
			self.agent.counter = 0

	def readStationsFromFile(self):
		"""
		Read list of available charging stations and update chargingStations
		"""

		self.chargingStations.clear()
		with open(self.stationListFilename, "r") as stationListFile:
			for stationLine in stationListFile:
				if stationLine in ["\n", "\r\n"]:
					continue
				(stationID, stationPositionX, stationPositionY) = stationLine.rstrip().split(",")
				stationTuple = (stationID, stationPositionX, stationPositionY)

				self.chargingStations.append(stationTuple)
		stationListFile.close()

	def __init__(self, *args, **kwargs):
		"""
		extra args:
		* stationListFilename: stationListFilename (default: "stationLocationList.csv")
		* updateFrequency: how frequently to update station list, in seconds (default: 1)
		* ttl: time-to-live, in seconds, zero means no ttl (default: 0)
		"""

		self.stationListFilename = kwargs.pop("stationListFilename", "stationLocationList.csv")
		self.updateFrequency = kwargs.pop("updateFrequency", 1)
		self.ttl = int(kwargs.pop("ttl", 0))

		self.chargingStations = []
		self.counter = 0

		# Can't use super() due to use of kwargs
		Agent.__init__(self, *args, **kwargs)

	def setup(self):
		print("[ServerAgent] setup!")

		# Cleaning stationLocationList file
		fileToClean = open(self.stationListFilename, "w+")
		fileToClean.close()

		# Add periodic UpdateStations Behaviour
		usBehaviour = self.UpdateStationsBehaviour(period=1)
		self.add_behaviour(usBehaviour)

		# Add GenerateStationList Behaviour (response to a message)
		gslTemplate = Template()
		gslTemplate.to = str(self.jid)
		gslTemplate.set_metadata("performative", "request")
		gslBehaviour = self.GenerateStationListBehaviour()
		self.add_behaviour(gslBehaviour, gslTemplate)
