"""
pyChargelio, the Python implementation of the Chargelio EV charging helper
"""

import time
from argparse import ArgumentParser
from pyChargelio.agents import ServerAgent, StationAgent, VehicleAgent


def runPyChargelio():
	"""
	Main entry point for pyChargelio.
	"""

	argumentParser = ArgumentParser(
		description="pyChargelio, the Python implementation of the Chargelio EV charging helper"
	)
	argumentParser.add_argument(
		"-a",
		"--agent",
		help="Agent type to spawn, available types: ev, cs, ls (EV, charging station, location server).",
	)
	argumentParser.add_argument(
		"-j",
		"--jid",
		help="XMPP JID for the agent. Defaults to <agent type>@localhost."
	)
	argumentParser.add_argument(
		"-p",
		"--password",
		help="XMPP password for the agent. Defaults to 'pass1234'.",
		default="pass1234"
	)
	argumentParser.add_argument(
		"-t",
		"--ttl",
		help="Time-to-live for charging station or location server agents. Defaults to 0 (no TTL).",
		default=0
	)
	argumentParser.add_argument(
		"--posX",
		help="X coordinate of vehicle's or station's position. Defaults to 0",
		default=0
	)
	argumentParser.add_argument(
		"--posY",
		help="Y coordinate of vehicle's or station's position. Defaults to 0",
		default=0
	)
	argumentParser.add_argument(
		"--chrStd",
		help="Supported charging standards for a station or a vehicle, comma-separated" +
		"(only 1st one applies for a station). Defaults to 'std1'",
		default="std1"
	)
	argumentParser.add_argument(
		"--serverJID",
		help="Location server agent's JID for vehicle agent. Defaults to 'ls@localhost",
		default="ls@localhost"
	)
	argumentParser.add_argument(
		"--radius",
		help="Station search radius for vehicle agent. Defaults to 10",
		default=10
	)
	argumentParser.add_argument(
		"--charge",
		help="Required charge (empty part of the battery) for the vehicle. Defaults to 50",
		default=50
	)
	argumentParser.add_argument(
		"--spots",
		help="Total number of charging spots for a station. Defaults to 5",
		default=5
	)
	argumentParser.add_argument(
		"--price",
		help="Charging price, ie price per unit of charge (kWh). Defaults to 0 (free)",
		default=0
	)
	argumentParser.add_argument(
		"--power",
		help="Charging power (kW). Defaults to 20000",
		default=20000
	)

	args = argumentParser.parse_args()

	xmppUserString = args.jid or (args.agent + "@localhost")

	if args.agent == "ev":
		agent = VehicleAgent(
			xmppUserString,
			args.password,
			vehiclePositionX=args.posX,
			vehiclePositionY=args.posY,
			chargingStandards=args.chrStd,
			serverAgentJID=args.serverJID,
			searchRadius=args.radius,
			requiredCharge=args.charge,
		)

	elif args.agent == "cs":
		# TODO: TTL
		agent = StationAgent(
			xmppUserString,
			args.password,
			positionX=args.posX,
			positionY=args.posY,
			chargingStandard=args.chrStd.split(',')[0],
			numberOfSpots=args.spots,
			chargingPrice=args.price,
			chargingPower=args.power,
		)

	elif args.agent == "ls":
		agent = ServerAgent(
			xmppUserString,
			args.password,
			ttl=args.ttl
		)

	else:
		print("Invalid agent type")
		argumentParser.print_usage()
		exit(1)

	agent.start()

	while agent.is_alive():
		try:
			time.sleep(0.1)
		except KeyboardInterrupt:
			agent.stop()
			break
