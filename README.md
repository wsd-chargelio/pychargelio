# pyChargelio

Python implementation of Chargelio agent-oriented EV charging helper.

<!-- MarkdownTOC -->

- [Installation](#installation)
	- [Pre-requirements](#pre-requirements)
	- [Code and dependencies](#code-and-dependencies)
- [Running](#running)
- [Contributing](#contributing)
	- [Language](#language)
	- [Coding style](#coding-style)
	- [Testing](#testing)

<!-- /MarkdownTOC -->

<a id="installation"></a>
## Installation
<a id="pre-requirements"></a>
### Pre-requirements
1. Python 3.6.x
	* check your current Python version with `python3 --version`, if you already have a Python 3.6.x, you can skip to step 2
	* install PyENV:
		* `curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash` (replace `bash` with your prefered shell)
		* Add the lines specified by the installer to your shell's configuration, e.g `.bashrc` or `.zshrc` (for Oh-My-Zsh there's a `pyenv` plugin)
		* Open new shell (or re-source the shell's configuration file)
	* Locally install Python 3.6.x (3.6.7 in this case)
		* `pyenv install 3.6.7` (this can take some time as it compiles the whole thing)
	* 'activate' the freshly-installed Python
		* `pyenv shell 3.6.7`
		* You can also make it default by issuing `pyenv local 3.6.7` (NOT recommended though).
2. PIP
	* If you've installed Python via Pyenv, skip this step (you already have the required PIP)
	* For Debian-based distros (Debian, Ubuntu, Mint etc): `sudo apt install python3-pip`
	* For RHEL7-based distros (RedHat, Fedora, CentOS etc): `sudo yum install python34-pip` (version might be slightly different, in case of package not found use `yum search python3 pip`)
	* For Arch-based distros: `sudo pacman -S python-pip`
3. Pipenv
	* `pip install pipenv`

<a id="code-and-dependencies"></a>
### Code and dependencies
1. pyChargelio code
	* open your directory of choice in terminal
	* `git clone git@gitlab.com:wsd-chargelio/pychargelio.git`
	* enter pyChargelio directory: `cd pychargelio`
2. Dependencies
	* (if needed) activate local Python installation (`pyenv shell 3.6.7`)
	* Pipenv stores the Python executable/path used for the installation, so only the first run requires the previous step
	* `pipenv install`
3. Development dependencies (for testing/linting/etc)
	* `pipenv install --dev`

<a id="running"></a>
## Running
* In the virtualenv
	* Open a shell in the virtualenv of the project: `pipenv shell`
	* Run commands as you would without the virtualenv, e.g. `python -m pyChargelio`
* Via `pipenv run`
	* Add the `pipenv run` prefix to your commands, e.g. `pipenv run python -m pyChargelio`
	* Or use pre-defined scripts/targets (located in Pipfile), e.g. `pipenv run pyChargelio`

<a id="contributing"></a>
## Contributing
<a id="language"></a>
### Language
Code must, obviously, be Python (with the small exception of configuration files, accompanying scripts etc).

Use English only.
Code, variables, classes, comments.
Consult a dictionary if needed.

<a id="coding-style"></a>
### Coding style
The coding style mostly adheres to PEP8, but with some notable differences:
* Use tabs instead of spaces for indentation
* Maximum line length increased to 120 (it's late 2018, not late '80s)
* All names should be either camelCase (packages, objects, variables, fields) or PascalCase (classes), not snake_case
* In classes, don't mix order of methods and subclasses - first subclasses, then methods
* Trim excessive whitespace - trailing, hanging, too many blank lines etc

To check whether your code adheres to the rules, simply run `pipenv run lint` (assuming you've installed development dependencies).
It should return 10/10.

<a id="testing"></a>
### Testing
TBD
