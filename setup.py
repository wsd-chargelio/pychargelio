from setuptools import setup

package_name = "pyChargelio"

setup(
	name = package_name,
	version = "0.0.1",
	author = "Chargelio team",
	maintainer = "Chargelio team",
	keywords = [
		"Chargelio",
		"SPADE",
		"agent-oriented",
		"charging",
	],
	classifiers = [
		"Programming Language :: Python",
	],
	description = (
		"Python implementation of Chargelio, the agent-oriented EV charging helper"
	),
	license = "",
	entry_points = {
		"console_scripts": [
			"pyChargelio = pyChargelio:run_pyChargelio",
		],
	},
)
